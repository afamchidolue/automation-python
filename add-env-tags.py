import boto3

ec2_client_us = boto3.client('ec2', region_name="us-east-1")
ec2_resource_us = boto3.resource('ec2', region_name="us-east-1")

ec2_client_canada = boto3.client('ec2', region_name="ca-central-1")
ec2_resource_canada = boto3.resource('ec2', region_name="ca-central-1")

instance_ids_us = []
instance_ids_canada = []

reservations_paris = ec2_client_us.describe_instances()['Reservations']
for res in reservations_paris:
    instances = res['Instances']
    for ins in instances:
        instance_ids_us.append(ins['InstanceId'])


response = ec2_resource_us.create_tags(
    Resources=instance_ids_us,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
)

reservations_frankfurt = ec2_client_canada.describe_instances()['Reservations']
for res in reservations_frankfurt:
    instances = res['Instances']
    for ins in instances:
        instance_ids_canada.append(ins['InstanceId'])


response = ec2_resource_canada.create_tags(
    Resources=instance_ids_canada,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
)
